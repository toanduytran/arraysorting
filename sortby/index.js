function sortBy(propGetter, comparer, next) {
  return (item1, item2) => {
    let result = comparer(propGetter(item1), propGetter(item2));
    if ((result === 0) && next) {
      return next(item1, item2);
    } 
    return result;
  }
}

function ascending(val1, val2) {
  if (val1 === val2) {
    return 0;
  } else if (val1 > val2) {
    return 1;
  }
  return -1;
}

function descending(val1, val2) {
  if (val1 === val2) {
    return 0
  } else if (val1 > val2) {
    return -1
  }
  return 1
}

module.exports = {
  sortBy: sortBy,
  ascending: ascending,
  descending: descending
};