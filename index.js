var sortBy = require('./sortby');
var Faker = require('Faker');

const generateArray = numberOfRows => {
  let arr = [];

  for (var i = 0; i < numberOfRows; i++) {
    let item = {
      firstName: Faker.Name.firstName(),
      lastName: Faker.Name.lastName(),
      age: 1 + Math.floor(Math.random() * 60)
    };

    arr.push(item);
  }

  return arr;
}

const sortingPeople = sortBy.sortBy( e => e.lastName, sortBy.ascending, 
  sortBy.sortBy( e => e.firstName, sortBy.ascending, 
  sortBy.sortBy( e => e.age, sortBy.descending
)));

let people = generateArray(10);

people.sort( sortingPeople );

let people2 = generateArray(1000000);

let start = new Date();
people2.sort( sortingPeople );
let stop = new Date();
console.log('Sorting 1000000 records took ', stop - start, 'ms');
